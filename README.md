# ssh-manage

[![builds.sr.ht status](https://builds.sr.ht/~vendion/ssh-manage.svg)](https://builds.sr.ht/~vendion/ssh-manage?)

The goal of `ssh-manage` is to be a simple tool to help manage user configuration
files for SSH. The host configurations are internally managed in a bbolt
database and can be written out to the configuration used by `ssh`.

## Installation

`ssh-manage` requires Go 1.17 or higher, and can be installed via `go install`:

    # Install a specific version.
    go install git.sr.ht/~vendion/ssh-manage@v0.3.0

    # Install the highest available version.
    go install git.sr.ht/~vendion/ssh-manage@latest

### Installing manually

`ssh-manage` can be cloned and built locally with the following:

    git clone https://git.sr.ht/~vendion/ssh-manage
    cd ssh-mangae
    make build

## Configuring

`ssh-manage` is designed to work out of the box making sane defaults. These
defaults can be changed by setting environment variables which can be
manually set or `ssh-manage` can load them on start up.

To have `ssh-manage` load these environment variables, first create
_~/.config/ssh-manage/ssh-manage.env_ and add the values to that file.

Here is a example file:

    SSH-PORT=2222

This tells `ssh-manage` to use port 2222 as the default SSH port.

## Quick Start

### Adding a host

    ssh-manage add

### Getting a host details

    ssh-manage get example

### Listing all hosts

    ssh-manage list

### Remove a host

    ssh-manage rm example

### Write the ssh configuration file

    ssh-manage write

### Update a host details

    ssh-manage update

### Import an existing ssh configuration file

    ssh-manage import

## License

`ssh-manage` is licensed under the [BSD 3-Clause "New" or "Revised" License](LICENSE).
