version != git describe --tags --always
files != find . -name '*.go'
gopath != go env GOPATH

build: ${files}
	@mkdir -p "${gopath}/bin"
	@go build -trimpath -ldflags \
		"-X git.sr.ht/~vendion/ssh-manage/pkg/buildinfo.Version=${version}" \
		-o "${gopath}/bin/ssh-manage" \
		git.sr.ht/~vendion/ssh-manage

lint: .golangci.yaml
	@golangci-lint run ./...

# This target depends on the following script: https://paste.sr.ht/~vendion/1e581780948d472cfbf4a010623c330d190e2a6f
changelog:
	@gen-changelog > CHANGELOG.md
	@git add CHANGELOG.md || true
	@git commit --signoff --message='Update CHANGELOG' || true

release: changelog build

.PHONY: build lint lint-fix changelog release
