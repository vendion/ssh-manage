// Package env contains information about the runtime environment.
package env

// Environment contains information about the environment that is being used.
type Environment struct {
	HostName string
	HomeDir  string
}
