// Copyright 2014 Adam Jimerson. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// Package database provides methods for working with the bbolt DB.
package database

import (
	"fmt"
	"strings"
)

// Host contains the configuration details for a SSH Host.
type Host struct {
	// Hostname or alias for the server
	Host []string
	// Fully qualified hostname or IP address to connect
	HostName string
	// Port number that the SSH daemon is listening on
	Port int
	// The name of the remote user to connect as
	User              string
	ProxyCommand      string
	HostKeyAlgorithms string
	// Path the SSH key that should be used when connecting
	IdentityFile string
	// Interval that the ServerKeepAlive command should be passed to the server
	KeepAlive int8
}

// String returns the host information as a string.
func (h Host) String() string {
	return fmt.Sprintf("%s %s %d %s %s %s %s %d",
		strings.Join(h.Host, ","), h.HostName, h.Port, h.User,
		h.ProxyCommand, h.HostKeyAlgorithms, h.IdentityFile, h.KeepAlive)
}

// Hosts is a slice of Host.
type Hosts []Host
