// Copyright 2014-2022 Adam Jimerson. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// Package database provides methods for working with the bbolt DB.
package database

import (
	"encoding/json"
	"fmt"
	"os"
	"path"
	"time"

	bolt "go.etcd.io/bbolt"
)

// DataStore provides an interface for working with the DB.
type DataStore interface {
	Write(string, []byte, []byte) error
	FetchAllItemsString(string) ([]string, error)
	GetHostInfo(string, []byte) (Host, error)
	FetchAll(string) (map[string]Host, error)
	RemoveHost(string, []byte) error
}

// DB is a wrapper around *bbolt.DB.
type DB struct {
	*bolt.DB
}

// Open opens a connection pool to the bbolt DB.
func Open(dbLoc, hostname string) (*DB, error) {
	dbFile, err := checkPathExists(dbLoc)
	if err != nil {
		return &DB{}, err //nolint:exhaustivestruct
	}

	//nolint:gomnd // The 0644 is the perms for the database if it needs to be created
	db, err := bolt.Open(
		dbFile,
		0o644,
		&bolt.Options{ //nolint:exhaustivestruct
			Timeout: 1 * time.Second,
		},
	)
	if err != nil {
		return nil, fmt.Errorf("failed to open DB: %w", err)
	}

	err = createBucketIfNotExist(db, hostname)

	return &DB{db}, err
}

func checkPathExists(dbLoc string) (string, error) {
	_, err := os.Stat(dbLoc)
	if err != nil && !os.IsNotExist(err) {
		return "", fmt.Errorf("failed opening database directory: %w", err)
	}

	if os.IsNotExist(err) {
		err := os.MkdirAll(dbLoc, 0o750) //nolint:gomnd // The 0750 is the perms for the directory containing the DB
		if err != nil {
			return "", fmt.Errorf("failed creating database directory: %w", err)
		}
	}

	dbfile := path.Join(dbLoc, "hosts.db")

	return dbfile, nil
}

func createBucketIfNotExist(db *bolt.DB, hostname string) error {
	return db.Update(func(tx *bolt.Tx) error { //nolint:wrapcheck // The error already is wrapped at this point
		_, err := tx.CreateBucketIfNotExists([]byte(hostname))
		if err != nil {
			return fmt.Errorf("failed to create bucket %s: %w", hostname, err)
		}

		return nil
	})
}

func (d *DB) Write(hostname string, key, val []byte) error {
	return d.Update(func(tx *bolt.Tx) error { //nolint:wrapcheck // The error already is wrapped at this point
		b := tx.Bucket([]byte(hostname))

		err := b.Put(key, val)
		if err != nil {
			return fmt.Errorf("failed to write host information to DB: %w", err)
		}

		return nil
	})
}

// FetchAllItemsString returns a slice of strings with all hosts in the DB.
func (d *DB) FetchAllItemsString(hostname string) ([]string, error) {
	var hosts []string

	d.View(func(tx *bolt.Tx) error { //nolint:errcheck,gosec // Fetching a list in this manner does not return an error
		// Assume bucket exists and has keys
		// the bucket should have been created at DB open time if it didn't already exist
		b := tx.Bucket([]byte(hostname))

		b.ForEach(func(k, v []byte) error { //nolint:errcheck,gosec // Fetching a list in this manner does not return an error
			hosts = append(hosts, fmt.Sprintf("%s: %s", string(k), string(v)))

			return nil
		})

		return nil
	})

	return hosts, nil
}

// GetHostInfo returns information on a single host entry from the DB.
func (d *DB) GetHostInfo(hostname string, host []byte) (Host, error) {
	var (
		h    Host
		info []byte
	)

	d.View(func(tx *bolt.Tx) error { //nolint:errcheck,gosec // The *Bucket.Get() method does not return an error
		// Assume bucket exists and has keys
		// the bucket should have been created at DB open time if it didn't already exist
		b := tx.Bucket([]byte(hostname))

		v := b.Get(host)
		info = v

		return nil
	})

	err := json.Unmarshal(info, &h)
	if err != nil {
		return Host{}, fmt.Errorf("failed to unmarshal data for host %s: %w", string(host), err)
	}

	return h, nil
}

// FetchAll returns a map containing information on all hosts in the DB.
func (d *DB) FetchAll(hostname string) (map[string]Host, error) {
	hosts := make(map[string]Host)

	err := d.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(hostname))

		err := b.ForEach(func(k, v []byte) error {
			var host Host
			key := string(k)

			err := json.Unmarshal(v, &host)
			if err != nil {
				return fmt.Errorf("failed to unmarshal host data: %w", err)
			}

			hosts[key] = host

			return nil
		})

		return err //nolint:wrapcheck // The error message if any gets wrapped below
	})
	if err != nil {
		return hosts, fmt.Errorf("failed to fetch list of hosts: %w", err)
	}

	return hosts, nil
}

// RemoveHost removes the named host from the DB.
func (d *DB) RemoveHost(hostname string, host []byte) error {
	err := d.Update(func(tx *bolt.Tx) error {
		return tx.Bucket([]byte(hostname)).Delete(host) //nolint:wrapcheck // The error is wrapped below
	})
	if err != nil {
		return fmt.Errorf("failed to delete host %s: %w", string(host), err)
	}

	return nil
}
