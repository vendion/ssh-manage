// Copyright 2014 Adam Jimerson. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package sshconfig

import (
	"context"
	"fmt"
	"path"

	"git.sr.ht/~vendion/ssh-manage/pkg/clog"
	"git.sr.ht/~vendion/ssh-manage/pkg/database"
	"git.sr.ht/~vendion/ssh-manage/pkg/env"
)

// ImportConf reads in the current $HOME/.ssh/config and stores the
// contents in the DB.
func ImportConf(ctx context.Context, db database.DataStore, env env.Environment) error {
	logger := clog.Logger(ctx)
	path := path.Join(env.HomeDir, `.ssh`, `config`)

	hosts, err := Import(path)
	if err != nil {
		return err
	}

	logger.Debugw("importing information from config file",
		"file", path,
		"hostname", env.HostName,
		"hosts", hosts)

	err = BackupSSHConfig(path)
	if err != nil {
		return fmt.Errorf("failed to backup current ssh config file: %w", err)
	}

	for i := range hosts {
		err = WriteToDB(db, env.HostName, hosts[i])
		if err != nil {
			return fmt.Errorf("failed to write parsed host entries to the DB: %w", err)
		}
	}

	return nil
}

// WriteConf writes out a new SSH config file with information from the DB.
func WriteConf(ctx context.Context, db database.DataStore, env env.Environment) error {
	path := path.Join(env.HomeDir, `.ssh`, `config`)

	err := BackupSSHConfig(path)
	if err != nil {
		return err
	}

	err = WriteNewConfig(db, env.HostName, path)
	if err != nil {
		return err
	}

	return nil
}
