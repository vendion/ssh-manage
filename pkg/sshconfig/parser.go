// Copyright 2014-2022 Adam Jimerson. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// Package sshconfig provides a way to work with the ssh user config file.
package sshconfig

import (
	"encoding/json"
	"fmt"

	sshcfg "github.com/mikkeloscar/sshconfig"

	"git.sr.ht/~vendion/ssh-manage/pkg/database"
)

// SSHHosts is a slice of sshconfig.SSHHost.
type SSHHosts []*sshcfg.SSHHost

// Import reads in the user's SSH config file from $HOME/.ssh/config and
// returns it's contents as SSHHosts, or returns an error.
func Import(filePath string) (SSHHosts, error) {
	hosts, err := sshcfg.ParseSSHConfig(filePath)
	if err != nil {
		return nil, fmt.Errorf("failed to parse ssh config file: %w", err)
	}

	return SSHHosts(hosts), nil
}

// WriteToDB writes a host entry to the DB.
func WriteToDB(db database.DataStore, hostname string, entry *sshcfg.SSHHost) error {
	//nolint:gomnd // KeepAlive defaults to 30
	host := database.Host{
		Host:              entry.Host,
		HostName:          entry.HostName,
		User:              entry.User,
		Port:              entry.Port,
		ProxyCommand:      entry.ProxyCommand,
		HostKeyAlgorithms: entry.HostKeyAlgorithms,
		IdentityFile:      entry.IdentityFile,
		KeepAlive:         30,
	}

	b, err := json.Marshal(host)
	if err != nil {
		return fmt.Errorf("failed to encode host information: %w", err)
	}

	nickName := host.Host[0]

	err = db.Write(hostname, []byte(nickName), b)
	if err != nil {
		return fmt.Errorf("failed to write host information to DB: %w", err)
	}

	return nil
}
