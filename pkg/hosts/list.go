// Copyright 2014-2022 Adam Jimerson. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package hosts

import (
	"context"
	"fmt"
	"strings"

	tcell "github.com/gdamore/tcell/v2"
	"github.com/rivo/tview"

	"git.sr.ht/~vendion/ssh-manage/pkg/clog"
	"git.sr.ht/~vendion/ssh-manage/pkg/database"
	"git.sr.ht/~vendion/ssh-manage/pkg/env"
)

// HostList lists all hosts in the system in a table.
func HostList(ctx context.Context, db database.DataStore, env env.Environment,
	app *tview.Application,
) (tview.Primitive, error) {
	pages := tview.NewPages()

	_, err := hostTable(ctx, db, env, app, pages)
	if err != nil {
		return pages, err
	}

	return pages, nil
}

//nolint: funlen
func hostTable(ctx context.Context, db database.DataStore, env env.Environment,
	app *tview.Application, pages *tview.Pages,
) (tview.Primitive, error) {
	logger := clog.Logger(ctx)
	table := tview.NewTable().SetBorders(true)

	hosts, err := db.FetchAll(env.HostName)
	if err != nil {
		return table, fmt.Errorf("failed fetching host information from the DB: %w", err)
	}

	logger.Debugf("returned list of hosts: %#v", hosts)

	cols, row := 9, 1

	printTableHeader(table)

	color := tcell.ColorWhite

	for key, val := range hosts {
		table.SetCell(row, 0,
			tview.NewTableCell(key).
				SetTextColor(color).
				SetAlign(tview.AlignLeft))

		word := 0
		host := strings.Split(val.String(), " ")

		for c := 1; c < cols; c++ {
			table.SetCell(row, c,
				tview.NewTableCell(host[word]).
					SetTextColor(color).
					SetAlign(tview.AlignLeft))

			word = (word + 1) % len(host)
		}

		row++
	}

	table.Select(0, 0).SetFixed(1, 1).SetDoneFunc(func(key tcell.Key) {
		switch key { //nolint:exhaustive
		case tcell.KeyEscape:
			app.Stop()
		case tcell.KeyEnter:
			table.SetSelectable(true, true)
		}
	}).SetSelectedFunc(func(row, column int) {
		cell := table.GetCell(row, 0)
		table.SetSelectable(false, false)

		hostInfoTbl, err := hostInfo(ctx, db, env, app, cell.Text)
		if err != nil {
			panic(err)
		}

		pages = pages.AddAndSwitchToPage("hostInfo", hostInfoTbl, true)
	})

	pages = pages.AddAndSwitchToPage("hostList", table, true)

	return table, nil
}

func printTableHeader(table *tview.Table) {
	headers := []string{
		"ID",
		"Host",
		"Hostname",
		"Port",
		"User",
		"Proxy command",
		"Host key algorithms",
		"Identity file",
		"Keep alive",
	}

	cols := len(headers)
	word := 0

	for c := 0; c < cols; c++ {
		table.SetCell(0, c,
			tview.NewTableCell(headers[word]).
				SetTextColor(tcell.ColorYellow).
				SetAlign(tview.AlignLeft))

		word = (word + 1) % len(headers)
	}
}
