// Copyright 2014 Adam Jimerson. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package hosts

import (
	"fmt"

	"git.sr.ht/~vendion/ssh-manage/pkg/database"
	"git.sr.ht/~vendion/ssh-manage/pkg/env"
)

// RemoveHost removes the given host from the data store.
func RemoveHost(db database.DataStore, env env.Environment, host string) error {
	err := db.RemoveHost(env.HostName, []byte(host))
	if err != nil {
		return fmt.Errorf("failed deleting host %s from the DB: %w", host, err)
	}

	return nil
}
