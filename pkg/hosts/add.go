// Copyright 2014-2022 Adam Jimerson. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// Package hosts manages SSH host entries.
package hosts

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"

	"github.com/rivo/tview"

	"git.sr.ht/~vendion/ssh-manage/pkg/database"
	"git.sr.ht/~vendion/ssh-manage/pkg/env"
)

// AddHostForm renders out the form to allow a user to enter in
// details about a new host entry.
func AddHostForm(db database.DataStore, env env.Environment, app *tview.Application) (form *tview.Form, err error) {
	defer func() {
		if r := recover(); r != nil {
			err = fmt.Errorf("%v", r) //nolint:goerr113
		}
	}()

	host := make(map[string]string)

	//nolint:gomnd // The third parameter for form.AddInputField is to set the field width.
	form = tview.NewForm().
		AddInputField("Nickname", "", 0, nil, func(val string) {
			host["nick"] = val
		}).
		AddInputField("Host", "", 0, nil, func(val string) {
			host["host"] = val
		}).
		AddInputField("Hostname/IP", "", 0, nil, func(val string) {
			host["hostname"] = val
		}).
		AddInputField("Port", "22", 5, nil, func(val string) {
			host["port"] = val
		}).
		AddInputField("User", "", 20, nil, func(val string) {
			host["user"] = val
		}).
		AddInputField("Proxy Command", "", 0, nil, func(val string) {
			host["proxy"] = val
		}).
		AddInputField("HostKey Algorithms", "", 0, nil, func(val string) {
			host["hostkey"] = val
		}).
		AddInputField("Identity File", "", 0, nil, func(val string) {
			host["identity"] = val
		}).
		AddInputField("Server Alive Interval", "30", 3, nil, func(val string) {
			host["alive"] = val
		}).
		AddButton("Save", func() {
			app.Stop() // we are done with the form so calling stop cleans things up
			err := saveNewEntry(db, env, host)
			if err != nil {
				panic(err)
			}
		}).
		AddButton("Quit", func() {
			app.Stop()
		})

	form.SetBorder(true).SetTitle("Add New Host Entry").SetTitleAlign(tview.AlignLeft)

	return form, nil
}

func saveNewEntry(db database.DataStore, env env.Environment, h map[string]string) error {
	var host database.Host

	host.Host = strings.Split(h["host"], " ")
	host.HostName = h["hostname"]

	i, err := strconv.Atoi(h["port"])
	if err != nil {
		i = 22
	}

	host.Port = i
	host.User = h["user"]
	host.ProxyCommand = h["proxy"]
	host.HostKeyAlgorithms = h["hostkey"]
	host.IdentityFile = h["identity"]

	i, err = strconv.Atoi(h["alive"])
	if err != nil {
		i = 30
	}

	host.KeepAlive = int8(i)

	var b []byte

	b, err = json.Marshal(host)
	if err != nil {
		return fmt.Errorf("failed to marshal host information: %w", err)
	}

	if err = db.Write(env.HostName, []byte(h["nick"]), b); err != nil {
		return fmt.Errorf("failed to add host information to database: %w", err)
	}

	return nil
}
