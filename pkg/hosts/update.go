// Copyright 2014-2022 Adam Jimerson. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package hosts

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"

	"github.com/rivo/tview"

	"git.sr.ht/~vendion/ssh-manage/pkg/database"
	"git.sr.ht/~vendion/ssh-manage/pkg/env"
)

// UpdateHostList lists all known hosts, and allows updating entries.
func UpdateHostList(db database.DataStore, env env.Environment, app *tview.Application) (tview.Primitive, error) {
	hosts := tview.NewList().ShowSecondaryText(false)
	hosts.SetBorder(true).SetTitle("Hosts")

	form := tview.NewForm()
	form.SetBorder(true).SetTitle("Update Host Entry").SetTitleAlign(tview.AlignLeft)
	flex := tview.NewFlex()

	hosts.SetDoneFunc(func() {
		app.Stop()
	})
	form.SetCancelFunc(func() {
		form.Clear(true)
		app.SetFocus(hosts)
	})

	err := buildHostList(db, env, app, hosts, form)
	if err != nil {
		return nil, err
	}

	// Create the layout.
	//nolint:gomnd // The second and third parameter of flex.AddItem controls sizing.
	flex.AddItem(hosts, 0, 1, true).
		AddItem(form, 0, 3, false)

	return flex, nil
}

//nolint:funlen
func buildHostList(db database.DataStore, env env.Environment, app *tview.Application,
	hosts *tview.List, form *tview.Form,
) (err error) {
	defer func() {
		if r := recover(); r != nil {
			err = fmt.Errorf("%v", r) //nolint:goerr113
		}
	}()

	h, _ := db.FetchAllItemsString(env.HostName)

	for i := range h {
		key := strings.Split(h[i], ": ")

		//nolint:gomnd // The third parameter for form.AddInputField is to set the field width.
		hosts.AddItem(key[0], "", 0, func() {
			// a host was selected so show its configuration
			updatedValues := make(map[string]string)
			form = form.Clear(true)
			var host database.Host
			err = json.Unmarshal([]byte(key[1]), &host)
			if err != nil {
				panic(fmt.Errorf("failed to unmarshal host information: %w", err))
			}

			form.AddInputField("Host", strings.Join(host.Host, " "), 0, nil, func(val string) {
				updatedValues["host"] = val
			})

			form.AddInputField("Hostname/IP", host.HostName, 0, nil, func(val string) {
				updatedValues["hostname"] = val
			})

			form.AddInputField("Port", strconv.Itoa(host.Port), 5, nil, func(val string) {
				updatedValues["port"] = val
			})

			form.AddInputField("User", host.User, 20, nil, func(val string) {
				updatedValues["user"] = val
			})

			form.AddInputField("Proxy Command", host.ProxyCommand, 0, nil, func(val string) {
				updatedValues["proxy"] = val
			})

			form.AddInputField("HostKey Algorithms", host.HostKeyAlgorithms, 0, nil, func(val string) {
				updatedValues["hostkey"] = val
			})

			form.AddInputField("Identity File", host.IdentityFile, 0, nil, func(val string) {
				updatedValues["identity"] = val
			})

			form.AddInputField("Server Alive Interval", strconv.Itoa(int(host.KeepAlive)), 3, nil, func(val string) {
				updatedValues["keepalive"] = val
			})

			form.AddButton("Cancel", func() {
				form.Clear(true)
				app.SetFocus(hosts)
			})

			form.AddButton("Update", func() {
				err = saveUpdatedEntry(db, env, key[0], host, updatedValues)
				if err != nil {
					return
				}

				form.Clear(true)
				h, _ = db.FetchAllItemsString(env.HostName)
				app.SetFocus(hosts)
			})

			app.SetFocus(form)
		})
	}

	return nil
}

//nolint:gocyclo,cyclop
func saveUpdatedEntry(db database.DataStore, env env.Environment, nickName string,
	orig database.Host, updates map[string]string,
) error {
	// updates will only contain things that have been changed
	if updates["host"] != "" {
		orig.Host = strings.Split(updates["host"], " ")
	}

	if updates["hostname"] != "" {
		orig.HostName = updates["hostname"]
	}

	if updates["port"] != "" {
		port, err := strconv.Atoi(updates["port"])
		if err != nil {
			port = 22
		}

		orig.Port = port
	}

	if updates["user"] != "" {
		orig.User = updates["user"]
	}

	if updates["proxy"] != "" {
		orig.ProxyCommand = updates["proxy"]
	}

	if updates["hostkey"] != "" {
		orig.HostKeyAlgorithms = updates["hostkey"]
	}

	if updates["identity"] != "" {
		orig.IdentityFile = updates["identity"]
	}

	if updates["keepalive"] != "" {
		alive, err := strconv.Atoi(updates["keepalive"])
		if err != nil {
			alive = 30
		}

		orig.KeepAlive = int8(alive)
	}

	b, err := json.Marshal(orig)
	if err != nil {
		return fmt.Errorf("failed to encode host information: %w", err)
	}

	if err = db.Write(env.HostName, []byte(nickName), b); err != nil {
		return fmt.Errorf("failed to update host information in database: %w", err)
	}

	return nil
}
