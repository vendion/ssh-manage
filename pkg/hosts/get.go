// Copyright 2014-2022 Adam Jimerson. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package hosts

import (
	"context"
	"fmt"

	tcell "github.com/gdamore/tcell/v2"
	"github.com/rivo/tview"

	"git.sr.ht/~vendion/ssh-manage/pkg/clog"
	"git.sr.ht/~vendion/ssh-manage/pkg/database"
	"git.sr.ht/~vendion/ssh-manage/pkg/env"
	"git.sr.ht/~vendion/ssh-manage/pkg/sshconfig"
)

// GetHost returns information on a single SSH host.
func GetHost(ctx context.Context, db database.DataStore, env env.Environment,
	app *tview.Application, host string,
) (tview.Primitive, error) {
	t, err := hostInfo(ctx, db, env, app, host)
	if err != nil {
		return nil, err
	}

	return t, nil
}

//nolint:funlen
func hostInfo(ctx context.Context, db database.DataStore, env env.Environment,
	app *tview.Application, host string,
) (tview.Primitive, error) {
	logger := clog.Logger(ctx)
	table := tview.NewTable().
		SetBorders(false).
		SetDoneFunc(func(key tcell.Key) {
			if key == tcell.KeyEscape {
				app.Stop()
			}
		})

	info, err := sshconfig.GetHostInfo(db, env.HostName, host)
	if err != nil {
		return table, fmt.Errorf("failed fetching host information from the DB: %w", err)
	}

	logger.Debugf("%#v", info)

	rows := []string{
		"ID",
		"Host",
		"HostName",
		"Port",
		"User",
		"Proxy Command",
		"Host Key Algorithms",
		"Identity File",
		"keep Alive",
	}

	for r := 0; r < 9; r++ {
		for c := 0; c < 2; c++ {
			cell := tview.NewTableCell(info[r])
			if c == 0 {
				cell = tview.NewTableCell(rows[r])
			}

			table.SetCell(r, c, cell)
		}
	}

	editBtn := tview.NewButton("Edit").SetSelectedFunc(func() {
		app.Stop()
		// UpdateHostList(db, env, app) // This is problematic, 1 it just takes the user to the list of known hosts,
		// 2 focus does not get updated correctly
	})
	editBtn.SetBorder(true).SetRect(0, 0, 22, 3)

	exitBtn := tview.NewButton("Quit").SetSelectedFunc(func() {
		app.Stop()
	})
	exitBtn.SetBorder(true).SetRect(0, 0, 22, 3)

	flex := tview.NewFlex().
		AddItem(tview.NewFlex().SetDirection(tview.FlexRow).
			AddItem(table, 0, 2, true).
			AddItem(tview.NewFlex().SetDirection(tview.FlexColumn).
				AddItem(editBtn, 0, 1, true).
				AddItem(exitBtn, 0, 1, true), 0, 1, true), 0, 1, true)

	return flex, nil
}
