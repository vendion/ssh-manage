// Copyright 2014 Adam Jimerson. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// Package buildinfo contains build information.
//
// Build information should be set during compilation by passing
// -ldflags "-X git.sr.ht/~vendion/ssh-manage/buildinfo.Var=value" to "go build" or
// "go get".
package buildinfo

// Version identifies the version of ssh-manage. On development commits, it
// identifies the next release.
//nolint:gochecknoglobals
var Version = "v0.4.0-dev"
