// Copyright 2014 Adam Jimerson. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// Package clog provides a context aware leveled logger.
package clog

import (
	"context"
	"log"
	"os"
	"path"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

type correlationIDType int

const (
	requestIDKey correlationIDType = iota
	sessionIDKey
)

var logger *zap.Logger //nolint:gochecknoglobals

// WithRqID returns a context which knows its requist ID.
func WithRqID(ctx context.Context, rqID string) context.Context {
	return context.WithValue(ctx, requestIDKey, rqID)
}

// WithSessionID returns a context which knows its session ID.
func WithSessionID(ctx context.Context, sessionID string) context.Context {
	return context.WithValue(ctx, sessionIDKey, sessionID)
}

// NewLogger returns a zap logger without any context to build from.
// Handles setting things like log level.
func NewLogger(ctx context.Context, lvl string) *zap.SugaredLogger {
	cfg := zap.NewProductionConfig()
	cfg.Encoding = "console"
	cfg.EncoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder

	var err error

	logger, err = cfg.Build(
		zap.Fields(
			zap.String("exe", path.Base(os.Args[0])),
		),
	)
	if err != nil {
		log.Fatalf("can't initialize zap logger: %v", err)
	}

	defer logger.Sync() // flushes buffer, if an

	switch lvl {
	case `debug`:
		cfg.Level.SetLevel(zap.DebugLevel)
	case `info`:
		cfg.Level.SetLevel(zap.InfoLevel)
	case `warn`:
		cfg.Level.SetLevel(zap.WarnLevel)
	case `error`:
		fallthrough
	default:
		cfg.Level.SetLevel(zap.ErrorLevel)
	}

	return logger.Sugar()
}

// Logger returns a zap logger with as much context as possible.
func Logger(ctx context.Context) *zap.SugaredLogger {
	newLogger := logger

	if ctx != nil {
		if ctxRqID, ok := ctx.Value(requestIDKey).(string); ok {
			newLogger = newLogger.With(zap.String("rqID", ctxRqID))
		}

		if ctxSessionID, ok := ctx.Value(sessionIDKey).(string); ok {
			newLogger = newLogger.With(zap.String("sessionID", ctxSessionID))
		}
	}

	return newLogger.Sugar()
}
