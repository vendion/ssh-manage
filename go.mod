module git.sr.ht/~vendion/ssh-manage

require (
	github.com/gdamore/tcell/v2 v2.4.1-0.20210905002822-f057f0a857a1
	github.com/mattn/go-runewidth v0.0.13 // indirect
	github.com/mikkeloscar/sshconfig v0.1.1
	github.com/rivo/tview v0.0.0-20220307222120-9994674d60a8
	github.com/subosito/gotenv v1.2.0
	go.etcd.io/bbolt v1.3.6
	go.uber.org/zap v1.21.0
	golang.org/x/sys v0.0.0-20220327210214-530d0810a4d0 // indirect
	golang.org/x/term v0.0.0-20210927222741-03fcf44c2211 // indirect
	golang.org/x/text v0.3.7 // indirect
)

require (
	github.com/gdamore/encoding v1.0.0 // indirect
	github.com/lucasb-eyer/go-colorful v1.2.0 // indirect
	github.com/mitchellh/go-homedir v1.1.0 // indirect
	github.com/rivo/uniseg v0.2.0 // indirect
	go.uber.org/atomic v1.9.0 // indirect
	go.uber.org/multierr v1.8.0 // indirect
)

go 1.17
