// Copyright 2014 Adam Jimerson. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"context"
	"os"

	"git.sr.ht/~vendion/ssh-manage/cmd/args"
	"git.sr.ht/~vendion/ssh-manage/cmd/config"
	"git.sr.ht/~vendion/ssh-manage/cmd/sshmanage"
	"git.sr.ht/~vendion/ssh-manage/pkg/clog"
	"git.sr.ht/~vendion/ssh-manage/pkg/env"
)

func main() {
	ctx := args.Parse(context.Background())

	logger := clog.NewLogger(ctx, args.GetOptions(ctx).Log)

	if err := config.Parse(ctx); err != nil {
		logger.Error(err.Error())
		os.Exit(1)
	}

	var (
		env env.Environment
		err error
	)

	env.HomeDir, err = config.GetHome()
	if err != nil {
		logger.Error(err.Error())
		os.Exit(1)
	}

	env.HostName, err = config.GetHostName()
	if err != nil {
		env.HostName = "default"
	}

	sshmanage.Execute(ctx, env)
}
