# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)

## [Unreleased]

## [v0.3.0] - 2021-08-04
- [Changed] Improve error and logging handling
- [Changed] Clean up the output from the list command
- [Added] Add ability to remove a host entry from the DB
- [Added] Added TUI interface for editing and adding host entries
- [Added] Added a command to write out a SSH config file
- [Added] Add a way to import an existing SSH config file into the DB
- [Changed] Switch from github.com/peterbourgon/diskv to bbolt for managing internal DB store.

## [v0.2.0] - 2015-02-08

## [v0.1] - 2014-03-30
