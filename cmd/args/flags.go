// Copyright 2014-2022 Adam Jimerson. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// Package args handles parsing command line arguments.
package args

import (
	"context"
	"flag"
	"fmt"
	"os"
	"path/filepath"

	"git.sr.ht/~vendion/ssh-manage/pkg/buildinfo"
)

// ContextCfg is the key used when getting/setting the Options struct from
// the context object.
type ContextCfg string

// Options contains all supported cmd line arguments.
type Options struct {
	// Log defines the logging level that should be used.
	Log string
	// Version displays the version information and exits.
	Version bool
}

// Parse reads any command line arguments given and acts on them.
func Parse(ctx context.Context) context.Context {
	opts := Options{
		Log:     "error",
		Version: false,
	}

	flag.StringVar(&opts.Log, `log`, `error`, `Set the log level`)
	flag.BoolVar(&opts.Version, `version`, false, `Display the version number and exit`)

	flag.Usage = Usage
	flag.Parse()

	if opts.Version {
		fmt.Printf("%s version %s\n", filepath.Base(os.Args[0]), buildinfo.Version)
		os.Exit(0)
	}

	k := ContextCfg(`options`)
	ctx = context.WithValue(ctx, k, opts)

	return ctx
}

// GetOptions fetches the stored configuration settings from the context.
func GetOptions(ctx context.Context) Options {
	k := ContextCfg(`options`)
	if v := ctx.Value(k); v != nil {
		return v.(Options) //nolint: forcetypeassert
	}

	return Options{} //nolint:exhaustivestruct
}

// Usage displays the usage information.
func Usage() {
	command := filepath.Base(os.Args[0])
	fmt.Fprintf(os.Stderr,
		`Usage: %s [options] {add|import|get|list|rm|write|update} [arguments]
%s requires one of the following commands:

add:	Add a new host record to the datastore
import:	Imports contents of ~/.ssh/config into the datastore
get:	Get details about a host record from the datastore
update:	Update an existing host record
list:	Lists all records in the datastore
rm:	Removes a record from the datastore
write:	Write out SSH configuration file

Options:
	-log:  Set the log level (default: error)
	-version: Display the version number and exits
	-help: Displays this help message
`, command, command)
}
