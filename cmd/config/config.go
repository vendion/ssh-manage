// Copyright 2014-2022 Adam Jimerson. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// Package config provides a way to work with runtime configuration options.
package config

import (
	"context"
	"errors"
	"fmt"
	"os"
	"os/user"
	"path"
	"strings"

	"github.com/subosito/gotenv"

	"git.sr.ht/~vendion/ssh-manage/pkg/clog"
)

// GetHome returns the home directory of the current user.
func GetHome() (string, error) {
	u, err := user.Current()
	if err != nil {
		return "", fmt.Errorf("faild getting current user information: %w", err)
	}

	return u.HomeDir, nil
}

// GetHostName returns the hostname for the current computer.
func GetHostName() (string, error) {
	host, err := os.Hostname()
	if err != nil {
		return "", fmt.Errorf("failed to get system hostname: %w", err)
	}

	return host, nil
}

// Return the path to where we should keep the data store.
func getConfigPath() (string, error) {
	var configDir string

	xdgHome := os.Getenv("XDG_CONFIG_HOME")

	home, err := GetHome()
	if err != nil {
		return "", fmt.Errorf("could not detect a valid HOME directory: %w", err)
	}

	switch {
	case xdgHome != "" && strings.HasPrefix(xdgHome, "/"):
		configDir = path.Join(xdgHome, "ssh-manage/")
	case home != "" && strings.HasPrefix(home, "/"):
		configDir = path.Join(home, ".config", "ssh-manage/")
	default:
		//nolint:goerr113 // There is no error to wrap
		return "", errors.New("could not detect valid XDG_CONFIG_HOME or HOME environment variables")
	}

	// if the configuration directory does not exist create it
	if err = os.MkdirAll(configDir, 0o750); err != nil { //nolint:gomnd // Sets the permissions for the dir
		return "", fmt.Errorf("could not make configuration directory: %w", err)
	}

	return configDir, nil
}

func loadConfig(envFile string) error {
	if err := gotenv.Load(envFile); err != nil {
		return fmt.Errorf("failed to load config file: %w", err)
	}

	return nil
}

// Parse loads in the environment variables used for runtime configuration.
func Parse(ctx context.Context) error {
	logger := clog.Logger(ctx)

	configDir, err := getConfigPath()
	if err != nil {
		return err
	}

	logger.Debugf("configuration directory: %s", configDir)

	envFile := path.Join(configDir, `ssh-manage.env`)

	_, err = os.Stat(envFile)
	if err == nil {
		if err := loadConfig(envFile); err != nil {
			logger.Warnw("could not parse config file", "config", envFile)
		}
	}

	logger.Warn("no configuration file found")

	return nil
}
