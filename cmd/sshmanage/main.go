// Copyright 2014-2022 Adam Jimerson. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// Package sshmanage is the main logic for the app.
package sshmanage

import (
	"context"
	"flag"
	"os"
	"path"

	"github.com/rivo/tview"

	"git.sr.ht/~vendion/ssh-manage/cmd/args"
	"git.sr.ht/~vendion/ssh-manage/pkg/clog"
	"git.sr.ht/~vendion/ssh-manage/pkg/database"
	"git.sr.ht/~vendion/ssh-manage/pkg/env"
	"git.sr.ht/~vendion/ssh-manage/pkg/hosts"
	"git.sr.ht/~vendion/ssh-manage/pkg/sshconfig"
)

// Execute starts the main flow of the app.
//nolint:funlen,gocyclo,cyclop
func Execute(ctx context.Context, env env.Environment) {
	logger := clog.Logger(ctx)

	db, err := database.Open(
		path.Join(env.HomeDir, ".config", "ssh-manage"),
		env.HostName)
	if err != nil {
		logger.Error(err.Error())
		os.Exit(1)
	}

	defer db.Close()

	if flag.NArg() == 0 {
		logger.Error("please supply a command")
		args.Usage()
		db.Close()
		os.Exit(1)
	}

	app := tview.NewApplication()

	var view tview.Primitive

	// TODO add the ability to set if a record or records should get
	// printed.  This needs to be host dependant.
	switch flag.Arg(0) {
	case "import":
		err = sshconfig.ImportConf(ctx, db, env)
	case "list":
		view, err = hosts.HostList(ctx, db, env, app)
	case "get":
		//nolint:gomnd // the get subcommand should only have 2 arguments.
		if flag.NArg() != 2 {
			logger.Error("no host specified to get information for")
			db.Close()
			os.Exit(1)
		}

		view, err = hosts.GetHost(ctx, db, env, app, flag.Arg(1))
	case "write":
		err = sshconfig.WriteConf(ctx, db, env)
	case "add":
		view, err = hosts.AddHostForm(db, env, app)
	case "update":
		view, err = hosts.UpdateHostList(db, env, app)
	case "rm":
		//nolint:gomnd // the rm subcommand should only have 2 arguments.
		if flag.NArg() != 2 {
			logger.Error("no host specified to remove")
			db.Close()
			os.Exit(1)
		}

		err = hosts.RemoveHost(db, env, flag.Arg(1))
	default:
		logger.Errorw("unknown command given", "command", flag.Arg(0))
		args.Usage()
		db.Close()
		os.Exit(1)
	}

	// Error handling for switch statement
	if err != nil {
		logger.Error(err.Error())
		db.Close()
		os.Exit(1)
	}

	if view != nil {
		if err := app.SetRoot(view, true).EnableMouse(true).Run(); err != nil {
			logger.Error(err.Error())
			db.Close()
			os.Exit(1)
		}
	}
}
